# Selenium 自动化测试框架

# （基于 python 3）

## 项目结构：

- **config**： 用来存储配置文件，如 config.ini文件，配种了所需浏览器方式，被测地址和登录需要的cookie

- framwork：框架底层封装层
      logger.py：封装日志

  ​                      base_page.py：封装selenium基本的方法

  ​                      browser_engine：封装浏览器层

- logs：测试过程中的日志文件

- screenshots：测试过程中错误截图文件


- pageobjects：封装了页面对象

- test_report：测试报告文件的输出


- testsuites：用于测试用例的存放和用例集合套件 
                        runtest2.py：执行脚本
                        loop_file.py：循环执行脚本 
                    
                    
                    
                    
                    
                    
                    其他说明：
                    
- 拉取最新的代码安装好python3.9或以上版本进入到相应文件夹中(cd 文件名)，

- 执行命令pip install -r requirements.txt

- 使用前，需安装下载谷歌浏览器和浏览器对应版本的驱动，下载后的驱动文件需要替换file文件中的文件和修改browser_engine中浏览器的路径运行runtest2.py文件

- 运行时需要修改配置文件中的cookie值，因除登录用例，其他的自动化用例都是使用cookie的方式进行的登录。



