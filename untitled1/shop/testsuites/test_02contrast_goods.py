# coding=utf-8
"""
对比商品
"""
import unittest
import time
from shop.pageobjects.select_brand import Select_brand
from shop.framework.browser_engine import BrowserEngine
from shop.pageobjects.contrast_goods import Contrast_goods
import configparser
import os.path


class Test_contrast_goods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        browser = BrowserEngine(cls)

        config = configparser.ConfigParser()

        file_path = os.path.dirname(os.path.abspath('.')) + '/config/config.ini'

        config.read(file_path)

        cookie_1 = config.get("testCookies", "cookie")

        cls.driver = browser.open_browser(cls)

        cls.driver.add_cookie({'name': 'PHPSESSID_RS', 'value': cookie_1})

        cls.driver.refresh()

        time.sleep(3)

    def test_01contrast_goods(self):

        brand = Select_brand(self.driver)

        contrast = Contrast_goods(self.driver)

        brand.switch_crafts()

        brand.click_my_brand()

        contrast.click_goods_el1()

        contrast.click_goods_el2()

        contrast.click_contrast_btn()

        contrast.click_goods_gz()

        c_1 = contrast.get_text_goods_gz()

        try:
            self.assertTrue(c_1)
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    def test_02goods_add(self):

        contrast = Contrast_goods(self.driver)

        contrast.click_goods_add()

    def test_03goods_del(self):
        contrast = Contrast_goods(self.driver)

        contrast.click_goods_del()

    def test_04goods_text(self):

        contrast = Contrast_goods(self.driver)

        contrast.click_goods_text()

        c_2 = contrast.get_text_goods_name()

        try:
            self.assertEqual(c_2, "3M 96号高效百洁布20片装")
            # self.assertEqual(c_2, "ANSELL/安思尔 93-843一次性丁腈橡胶手套 XS")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    @classmethod
    def tearDownClass(cls):
        time.sleep(3)
        cls.driver.quit()
