# coding=utf-8
"""
商品详情页面，包括下单流程
"""
import unittest
import time
from shop.pageobjects.goods import Goods
from shop.framework.browser_engine import BrowserEngine
import configparser
import os.path


class Test_goods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        browser = BrowserEngine(cls)

        config = configparser.ConfigParser()

        file_path = os.path.dirname(os.path.abspath('.')) + '/config/config.ini'

        config.read(file_path)

        cookie_1 = config.get("testCookies", "cookie")

        cls.driver = browser.open_browser(cls)

        cls.driver.add_cookie({'name': 'PHPSESSID_RS', 'value': cookie_1})

        cls.driver.refresh()

        time.sleep(3)

    def test_01click_collect(self):
        foods = Goods(self.driver)

        foods.switch_crafts()

        foods.click_my_brand()

        foods.click_good_pict()

        foods.click_good_collect()

        # time.sleep(3)

        f = foods.get_text_collect_true()

        try:
            self.assertIn(f, "已关注")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    def test_02click_good_pict(self):
        foods = Goods(self.driver)
        foods.click_good_add_cart()

    def test_03type_good_num(self):
        foods = Goods(self.driver)

        foods.type_good_num("2")

        foods.click_good_buy()

        foods.click_ship_free()

        foods.click_add_order()

        f_1 = foods.get_text_add_true()

        try:
            self.assertEqual(f_1, "订单提交成功！")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    @classmethod
    def tearDownClass(cls):
        time.sleep(3)
        cls.driver.quit()





