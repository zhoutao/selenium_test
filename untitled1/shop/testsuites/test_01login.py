# coding=utf-8
"""
登录
"""
import unittest
import time
from shop.pageobjects.login import Login
from shop.framework.browser_engine import BrowserEngine


class Test_login(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        browser = BrowserEngine(cls)
        cls.driver = browser.open_browser(cls)

    def test_01click_login_t(self):
        login = Login(self.driver)

        login.click_login_t()

        login.type_phone("17863962858")

        login.type_pwd("qqqqqq1")

        login.click_login_btn()

    # @unittest.skip("跳过登录")
    def test_02get_text_phone(self):
        login = Login(self.driver)
        test_a = login.get_text_phone()
        try:
            self.assertEqual(test_a, "17863962858")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    @classmethod
    # @unittest.skip("跳过登录")
    def tearDownClass(cls):
        time.sleep(4)
        cls.driver.quit()

