# coding=utf-8
"""
商品操作：关注，加入购物车，跳转商品详情页面
"""
import unittest
import time
from shop.pageobjects.select_brand import Select_brand
from shop.framework.browser_engine import BrowserEngine
from shop.pageobjects.goods_operating import Goods_operaing
import configparser
import os.path


class Test_goods_operating(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        browser = BrowserEngine(cls)

        config = configparser.ConfigParser()

        file_path = os.path.dirname(os.path.abspath('.')) + '/config/config.ini'

        config.read(file_path)

        cookie_1 = config.get("testCookies", "cookie")

        cls.driver = browser.open_browser(cls)

        cls.driver.add_cookie({'name': 'PHPSESSID_RS', 'value': cookie_1})

        cls.driver.refresh()

        time.sleep(3)

    def test_01goods_gz(self):

        brand = Select_brand(self.driver)

        contrast = Goods_operaing(self.driver)

        brand.switch_crafts()

        brand.click_my_brand()

        contrast.click_goods_gz2()

        time.sleep(1)

    def test_02goods_join_cart(self):

        contrast = Goods_operaing(self.driver)

        contrast.click_goods1_join_cart()

        contrast.click_join_cart_close()

    def test_03goods2_join_cart(self):

        contrast = Goods_operaing(self.driver)

        contrast.click_goods2_join_cart()

        contrast.click_join_cart_close1()

        contrast.click_goods3_join_cart()

        contrast.click_join_cart_close2()

    def test_04goods_details(self):

        contrast = Goods_operaing(self.driver)

        contrast.click_goods_details()

    def test_05goods_name(self):

        contrast = Goods_operaing(self.driver)

        name = contrast.get_goods_name()

        try:
            self.assertEqual(name, "3M 4100白色抛光垫17英寸")
            # self.assertEqual(c_2, "ANSELL/安思尔 93-843一次性丁腈橡胶手套 XS")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    @classmethod
    def tearDownClass(cls):
        time.sleep(4)
        cls.driver.quit()


