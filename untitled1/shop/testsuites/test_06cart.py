# coding=utf-8
"""
购物车操作
"""
import unittest
import time
from shop.pageobjects.goods import Goods
from shop.framework.browser_engine import BrowserEngine
from shop.pageobjects.cart import Cart
import configparser
import os.path


class Test_cart(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        browser = BrowserEngine(cls)

        cls.driver = browser.open_browser(cls)

        config = configparser.ConfigParser()

        file_path = os.path.dirname(os.path.abspath('.')) + '/config/config.ini'

        config.read(file_path)

        cookie_1 = config.get("testCookies", "cookie")

        cls.driver.add_cookie({'name': 'PHPSESSID_RS', 'value': cookie_1})

        cls.driver.refresh()

        time.sleep(3)

    def test_01click_my_cart(self):
        cart = Cart(self.driver)

        cart.click_my_cart()

        cart.click_select()

    def test_02goods_num(self):
        cart = Cart(self.driver)

        c_1 = cart.get_text_goods_true()

        cart.click_num_add()

        try:
            self.assertTrue(int(c_1)+1 == int(cart.get_text_goods_true()))
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

        cart.click_num_less()

        try:
            self.assertTrue(int(c_1) == int(cart.get_text_goods_true()))
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    def test_03goods_del(self):
        cart = Cart(self.driver)

        cart.click_good_del1()

    def test_04goods_gz1(self):
        cart = Cart(self.driver)

        cart.click_good_gz1()

        cart.click_cart_btn()

    def test_05add_goods_order(self):
        goods = Goods(self.driver)

        goods.click_ship_free()

        goods.click_add_order()

        f_1 = goods.get_text_add_true()

        try:
            self.assertEqual(f_1, "订单提交成功！")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    @classmethod
    def tearDownClass(cls):
        time.sleep(4)
        cls.driver.quit()







