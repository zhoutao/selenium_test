# coding=utf-8
"""
现货商品，自提门店下单流程
"""
import unittest
import time
from shop.pageobjects.goods import Goods
from shop.framework.browser_engine import BrowserEngine
from shop.pageobjects.spot_goods import Spot_goods
import configparser
import os.path


class Test_spot_goods(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        browser = BrowserEngine(cls)

        cls.driver = browser.open_browser(cls)

        config = configparser.ConfigParser()

        file_path = os.path.dirname(os.path.abspath('.')) + '/config/config.ini'

        config.read(file_path)

        cookie_1 = config.get("testCookies", "cookie")

        cls.driver.add_cookie({'name': 'PHPSESSID_RS', 'value': cookie_1})

        cls.driver.refresh()

        time.sleep(3)

    def test_01buy_spot_goods(self):
        spot = Spot_goods(self.driver)

        spot.switch_crafts()

        spot.click_my_brand1()

        spot.click_good_pict1()

        spot.click_good_buy1()

    def test_02store(self):
        spot = Spot_goods(self.driver)

        spot.click_store()

        spot.click_store1()

    def test_03leave(self):
        spot = Spot_goods(self.driver)

        spot.type_leave("测试订单留言")

    def test_04add_goods_order(self):
        spot = Spot_goods(self.driver)

        spot.click_goods_btn1()

        # time.sleep(3)

        s_1 = spot.get_text_add_true1()

        try:
            self.assertEqual(s_1, "订单提交成功！")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    @classmethod
    def tearDownClass(cls):
        time.sleep(4)
        cls.driver.quit()

