# coding=utf-8

import unittest
import os
from HTMLTestRunner import HTMLTestRunner
import time
from HTMLTestReportCN import HTMLTestRunner
from shop.testsuites.test_01login import Test_login
from shop.testsuites.test_04select_brand import Test_select_brand
from shop.testsuites.test_05goods import Test_goods
from shop.testsuites.test_02contrast_goods import Test_contrast_goods
from shop.testsuites.test_06cart import Test_cart
from shop.testsuites.test_03goods_operating import Test_goods_operating
from shop.testsuites.test_07spot_goods import Test_spot_goods
from shop.testsuites.test_08spot_goods_Logistics import Test_spot_goods_logistics

# 获取TestSuite的实例化对象
# suite = unittest.TestSuite()
#
# loader = unittest.TestLoader()
#
# tests_1 = [Test_cart("test_01click_my_cart"),
#            Test_cart("test_02goods_num"),
#            Test_cart("test_03goods_del"),
#            Test_cart("test_04goods_gz1"),
#            Test_cart("test_05add_goods_order"),
#            ]
# # tests_2 = [Test_cart("test_01click_my_cart"), Test_cart("test_02goods_num"), Test_cart("test_03goods_del"),
# #            Test_cart("test_04goods_gz1"), Test_cart("test_05add_goods_order"),
# #            ]
# # suite.addTests(tests_2)
#
# suite.addTests(tests_1)


project_path = os.path.abspath(os.path.join(os.path.dirname(os.path.split(os.path.realpath(__file__))[0]), '.'))

test_case_path = project_path+"\\testsuites"

suite = unittest.defaultTestLoader.discover(start_dir=test_case_path, pattern='test*.py')

# 测试报告
xxx_title = "商城测试报告"

# 报告存储路径
report_url = os.path.dirname(os.path.abspath('')) + '/test_report/'

# 获取当前时间
now = time.strftime("%Y-%m-%d-%H_%M_%S", time.localtime(time.time()))

#  设置报告名称格式
HtmlFile = report_url + now + "HTMLtemplate.html"

# print(HtmlFile)

# 判断report_url里的文件是否存在的意思，括号内的可以是文件路径。

isExists = os.path.exists(report_url)

if not isExists:
    try:
        os.makedirs(report_url)
    except Exception as e:
        print("创建文件夹失败", e)

if __name__ == '__main__':
    # 获取TestSuite的实例化对象

    with open(HtmlFile, "wb") as report:
        runner = HTMLTestRunner(stream=report, title=xxx_title, description=u"测试结果")
        runner.run(suite)
