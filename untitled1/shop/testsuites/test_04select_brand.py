# coding=utf-8
"""
商品分类列表，部分功能操作
"""
import unittest
import time
from shop.pageobjects.select_brand import Select_brand
from shop.framework.browser_engine import BrowserEngine
from shop.pageobjects.filter_goods import Filter_goods


class Test_select_brand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        browser = BrowserEngine(cls)
        cls.driver = browser.open_browser(cls)

    def test_01click_my_brand(self):
        brand = Select_brand(self.driver)

        brand.switch_crafts()

        brand.click_my_brand()

        brand.click_more()

        brand.click_more_select()

        brand.click_any_brand_1()

        brand.click_any_brand_2()

        brand.click_brand_btn()

    def test_02click_price(self):
        filter_1 = Filter_goods(self.driver)

        filter_1.click_price()

    def test_03filter_price(self):
        filter_1 = Filter_goods(self.driver)

        filter_1.type_price_large("1")

        filter_1.type_price_little("5")

        filter_1.click_price_btn()

        filter_1.get_text_goods_one()

        filter_1.get_text_goods_two()

        filter_1_one = filter_1.get_text_goods_one()

        filter_1_two = filter_1.get_text_goods_two()
        try:
            self.assertLess(filter_1_one, filter_1_two)
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:

            pass

    def test_04view_list(self):
        filter_1 = Filter_goods(self.driver)

        filter_1.click_list_1()

    def test_05view_big_page(self):
        filter_1 = Filter_goods(self.driver)

        filter_1.click_big_page()

    @classmethod
    def tearDownClass(cls):
        time.sleep(4)
        cls.driver.quit()
