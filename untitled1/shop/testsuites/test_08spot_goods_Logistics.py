# coding=utf-8
"""
 现货商品,物流配送下单流程
"""
import unittest
import time
from shop.pageobjects.goods import Goods
from shop.framework.browser_engine import BrowserEngine
from shop.pageobjects.spot_goods import Spot_goods
from shop.pageobjects.spot_goods_logistics import Spot_goods_Logistics
import configparser
import os.path


class Test_spot_goods_logistics(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        browser = BrowserEngine(cls)

        cls.driver = browser.open_browser(cls)

        config = configparser.ConfigParser()

        file_path = os.path.dirname(os.path.abspath('.')) + '/config/config.ini'

        config.read(file_path)

        cookie_1 = config.get("testCookies", "cookie")

        cls.driver.add_cookie({'name': 'PHPSESSID_RS', 'value': cookie_1})

        cls.driver.refresh()

        time.sleep(3)

    def test_01buy_spot_goods_logistics(self):
        spot = Spot_goods(self.driver)

        spot.switch_crafts()

        spot.click_my_brand1()

        spot.click_good_pict1()

        spot.click_good_buy1()

    def test_02goods_logistics(self):

        goods_logistics = Spot_goods_Logistics(self.driver)

        goods_logistics.click_logistics()

        goods_logistics.click_city_btn()

        goods_logistics.click_city_province()

        goods_logistics.click_city()

        goods_logistics.click_area()

        goods_logistics.click_logistics_address()

        goods_logistics.click_logistics_address1()

        goods_logistics.click_logistics_address_btn()

    def test_03click_forwarding_goods(self):
        goods_logistics = Spot_goods_Logistics(self.driver)

        goods_logistics.click_forwarding()

        goods_logistics.click_forwarding_user()

    def test_04add_spot_goods_order(self):
        goods_logistics = Spot_goods_Logistics(self.driver)

        goods_logistics.click_goods_btn2()

        s_1 = goods_logistics.get_text_add_true2()

        try:
            self.assertEqual(s_1, "订单提交成功！")
        except AssertionError as a:
            print("Comparison failed")
            raise a
        else:
            pass

    @classmethod
    def tearDownClass(cls):
        time.sleep(4)
        cls.driver.quit()



