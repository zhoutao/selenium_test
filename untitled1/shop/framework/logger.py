# -*- coding:utf-8 -*-
import logging
import time
import os.path


class Logger(object):

    def __init__(self, logger):

        # 创建一个logger
        self.logger = logging.getLogger(logger)
        self.logger.setLevel(logging.INFO)

        # 设置日志存放路径，日志文件名
        # 获取本地时间，转换为设置的格式
        rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))

        log_path = os.path.dirname(os.path.abspath('.'))+'/logs/'
        # log_path = os.path.dirname(os.path.abspath('.'))+'/untitled1/shop/logs/'

        # 如果case组织结构式 /testsuit/featuremodel/xxx.py ， 那么得到的相对路径的父路径就是项目根目录
        log_name = log_path + rq + '.log'
        fh = logging.FileHandler(log_name)
        fh.setLevel(logging.INFO)

        # 再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)

        # 定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # 给logger添加handler
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def getlog(self):
        return self.logger





