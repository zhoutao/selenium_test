# coding=utf-8
from selenium import webdriver
import time
from shop.framework.logger import Logger
import os.path
from selenium.common.exceptions import NoSuchElementException

from selenium.webdriver.support.select import Select

logger = Logger(logger="BasePage").getlog()


class BasePage(object):
    def __init__(self, driver):

        self.driver = driver

    # 静态方法 类或实例均可调用,该静态方法函数里不传入self 或 cls
    @staticmethod
    def isdisplayed(element):
        value = element.isdisplayed()
        return value

    def back(self):
        """
         浏览器后退按钮
        """
        self.driver.back()

    def forward(self):
        """
        浏览器前进按钮
        """
        self.driver.forward()
        logger.info("点击当前页前进")

    def wait(self, seconds):
        """
        隐式等待
        :rtype: object
        """
        self.driver.implicitly_wait(seconds)
        logger.info("等待 %d 秒" % seconds)

    def sleep(self, seconds):
        """
        强制等待
        """
        time.sleep(seconds)
        logger.info("等待 %d 秒" % seconds)

    def close(self):
        """
        关闭当前窗口
        """
        try:
            self.driver.close()
            logger.info("关闭窗口成功")
        except NameError as e:
            logger.error("无法通过以下方式退出浏览器 %s" % e)

    def find_element(self, selector):
        """
        定位元素
        "x,//*[@id='langs']/button"
        "i,xxx"
        """

        if ',' not in selector:
            return self.driver.find_element_by_id(selector)
        selector_by = selector.split(',')[0]
        selector_value = selector.split(',')[1]

        if selector_by == "i" or selector_by == 'id':
            element = self.driver.find_element_by_id(selector_value)
        elif selector_by == "n" or selector_by == 'name':
            element = self.driver.find_element_by_name(selector_value)
        elif selector_by == "c" or selector_by == 'class_name':
            element = self.driver.find_element_by_class_name(selector_value)
        elif selector_by == "l" or selector_by == 'link_text':
            element = self.driver.find_element_by_link_text(selector_value)
        elif selector_by == "p" or selector_by == 'partial_link_text':
            element = self.driver.find_element_by_partial_link_text(selector_value)
        elif selector_by == "t" or selector_by == 'tag_name':
            element = self.driver.find_element_by_tag_name(selector_value)
        elif selector_by == "x" or selector_by == 'xpath':
            try:
                element = self.driver.find_element_by_xpath(selector_value)
                # logger.info("元素定位成功，定位方式为：%s,使用的值为%s:" % (selector_by, selector_value))
            except NoSuchElementException as e:
                logger.error("没有定位到元素： %s" % e)
                self.get_windows_img()
        elif selector_by == "s" or selector_by == 'selector_selector':
            element = self.driver.find_element_by_css_selector(selector_value)
        else:
            logger.error("没有找到元素")
            self.get_windows_img()
        # raise NameError("Please enter a valid type of targeting elements.")
        logger.info("元素定位成功，定位方式为：%s,使用的值为%s:" % (selector_by, selector_value))
        return element

    def type(self, selector, text):
        """
        输入
        driver.type("i,el","selenium")
        """
        el = self.find_element(selector)
        el.clear()
        try:
            el.send_keys(text)
            logger.info("在输入框内输入 \' %s \' " % text)
        except NameError as e:
            logger.error("Failed to type in input box with %s" % e)
            self.get_windows_img()

    def click(self, selector):
        """
        点击元素

        """
        el = self.find_element(selector)
        try:
            el.click()
            logger.info("点击元素成功")
        except NameError as e:
            logger.error("无法点击的元素 %s" % e)

    # 获取网页标题
    def get_page_title(self):
        logger.info("获取网页标题成功 %s" % self.driver.title)
        return self.driver.title

    def get_windows_img(self):
        """
        file_path这个参数写死，直接保存到根目录的一个文件夹.\Screenshots下
        """
        file_path = os.path.dirname(os.path.abspath('.')) + '/screenshots/'
        rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
        screen_name = file_path + rq + '.png'
        try:
            self.driver.get_screenshot_as_file(screen_name)
            logger.info("已经截图并保存到文件夹 : /screenshots")
        except NameError as e:
            logger.error("无法截图！ %s" % e)
            self.get_windows_img()

    def get_text(self, selector):
        """

        获取元素值，并返回
        """
        el = self.find_element(selector)
        try:
            time.sleep(3)
            # el.get_attribute("textContent")
            a = el.text
            logger.info("获取元素的值成功 %s" % a)
            # print(a)
            return a
        except NameError as e:
            logger.error("没有获取到元素的值 %s" % e)

    def get_text2(self, selector):
        """

        获取元素值，并返回
        """
        el = self.find_element(selector)
        try:
            time.sleep(3)
            # el.get_attribute("textContent")
            a = el.get_attribute('value')
            logger.info("获取元素的值成功 %s" % a)
            # print(a)
            return a
        except NameError as e:
            logger.error("没有获取到元素的值 %s" % e)

    def switch_window(self):
        """

        切换窗口
        """
        currentwin = self.driver.current_window_handle

        handles = self.driver.window_handles

        for i in handles:
            if i != currentwin:
                print(i)
                self.driver.switch_to.window(i)

    def under(self):
        """
        下拉到底部
        """
        i = 100
        try:

            for i in range(2, 90):
                js = "var q=document.documentElement.scrollTop={}".format(i * 100)
                self.driver.execute_script(js)
            time.sleep(2)
            # driver.find_element_by_css_selector(".goods-collect-22584").click()
        except NameError:
            print("下拉失败")

    def top(self):
        """
        上拉到顶部
        """

        try:
            js = "window.scrollTo(0,0)"

            self.driver.execute_script(js)

            time.sleep(2)
            # driver.find_element_by_css_selector(".goods-collect-22584").click()
        except NameError:
            print("上拉失败")

    def sliding(self):
        """
        上拉到顶部
        """

        try:
            js_1 = "var q=document.documentElement.scrollTop=500"
            self.driver.execute_script(js_1)

            time.sleep(2)
            # driver.find_element_by_css_selector(".goods-collect-22584").click()
        except NameError:
            print("滑动失败")







