from selenium import webdriver
from shop.framework.logger import Logger
import configparser
import os.path
from selenium.webdriver.chrome.options import Options

options = Options()

options.binary_location = r'C:\Users\Administrator\AppData\Local\Google\Chrome\Application\chrome.exe'

# path = r'C:\Users\Administrator\AppData\Local\Google\Chrome\Application\chromedriver.exe'
dir = os.path.dirname(os.path.abspath('.'))   # 获取相对路径

path = dir + '/file/chromedriver.exe'

logger = Logger(logger="browser_engine").getlog()


class BrowserEngine(object):

    def __init__(self, driver):
        self.driver = driver

    def open_browser(self, driver):
        config = configparser.ConfigParser()
        file_path = os.path.dirname(os.path.abspath('.')) + '/config/config.ini'
        config.read(file_path)

        browser = config.get("browserType", "browserName")
        logger.info("You had select %s browser." % browser)
        url = config.get("testServer", "URL")
        logger.info("The test server url is: %s" % url)

        if browser == 'Firefox':
            driver = webdriver.Firefox()
            logger.info("打开浏览器为火狐浏览器")
        elif browser == 'IE':
            driver = webdriver.Ie()
            logger.info("打开浏览器为IE浏览器")
        elif browser == "Chrome":
            driver = webdriver.Chrome(chrome_options=options, executable_path=path)
            logger.info("打开浏览器为Chrome")

        driver.get(url)
        logger.info("打开链接: %s" % url)
        # 浏览器全屏显示
        driver.maximize_window()
        logger.info("浏览器全屏显示")
        # 隐性等待10s
        driver.implicitly_wait(10)
        return driver
