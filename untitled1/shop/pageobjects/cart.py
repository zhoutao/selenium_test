# coding=utf-8
from shop.framework.base_page import BasePage


class Cart(BasePage):
    """
    购物车：
    点击购物车
    点击全选按钮
    点击任意商品后面的+号，增加数量
    点击任意商品后面的-号，增加数量
    点击删除按钮
    点击关注按钮
    选择商品结算
    选择发货方式
    点击提交订单
    """

    # 点击购物车
    my_cart = "s,.header_cartBox > span:nth-child(2)"

    my_cart1 = "s,.go_payr > a"

    # 点击全选按钮
    select = "s,.table-title .el-checkbox__inner"

    # 点击任意商品后面的+号，增加数量
    num_add = "x,//span[2]/i"

    # 点击任意商品后面的-号，减少数量
    num_less = "x,//span/div/span"

    # 删除商品
    good_del1 = "s,.table-row:nth-child(1) > div:nth-child(2) .table-btn:nth-child(1)"

    good_del2 = "s,.el-dialog__footer:nth-child(3) .el-button--default"

    # 关注商品
    good_gz1 = "s,.table-row:nth-child(1) > div:nth-child(2) .table-btn:nth-child(2)"

    # 结算商品
    cart_btn = "s,.sectionC"

    # 判断输入框中的数量
    num_true = "x,//div/input"

    # 点击购物车
    def click_my_cart(self):
        self.click(self.my_cart)

        self.click(self.my_cart1)

        self.sleep(2)

    # 点击全选按钮
    def click_select(self):
        self.click(self.select)

    # 点击任意商品后面的+号，增加数量
    def click_num_add(self):
        self.click(self.num_add)

    # 点击任意商品后面的-号，减少数量
    def click_num_less(self):
        self.click(self.num_less)

    # 删除商品
    def click_good_del1(self):

        self.sleep(3)

        self.click(self.good_del1)

        self.click(self.good_del2)

    # 关注商品
    def click_good_gz1(self):

        self.sleep(2)

        self.click(self.good_gz1)

        self.sleep(2)

    # 点击立即结算
    def click_cart_btn(self):

        self.under()

        self.click(self.cart_btn)

        self.sleep(5)

    # 判断输入框中的数量
    def get_text_goods_true(self):

        return self.get_text2(self.num_true)




