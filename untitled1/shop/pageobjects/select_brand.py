# coding=utf-8
from shop.framework.base_page import BasePage


class Select_brand(BasePage):
    """
    商品分类/选择品牌
    操作步骤：
    点击清洁，点击更多，点击多选品牌，点击任意品牌，点击确定查看筛选后的结果

    """
    # 定位器

    # 切换工艺品
    crafts = "s,.banner_list > .banner_title"

    # 清洁
    my_brand = "s,.a-center:nth-child(2) > a:nth-child(2)"

    # 更多
    more = "s,.brand-mores"

    # 多选
    more_select = "s,.brand-more > span"

    # 任意品牌
    any_brand_1 = "s,.el-checkbox:nth-child(1) > .el-checkbox__label"

    any_brand_2 = "s,.el-checkbox:nth-child(2) > .el-checkbox__label"

    # 确定
    brand_btn = "s,.confirm-b"

    # 切换工艺品
    def switch_crafts(self):
        self.click(self.crafts)

        self.switch_window()

        self.sleep(1)

    # 点击个人防护
    def click_my_brand(self):
        self.click(self.my_brand)

        self.sleep(2)

    # 点击更多
    def click_more(self):

        self.switch_window()

        self.click(self.more)

    # 点击多选
    def click_more_select(self):
        self.click(self.more_select)

    # 点击任意品牌
    def click_any_brand_1(self):
        self.click(self.any_brand_1)

    def click_any_brand_2(self):
        self.click(self.any_brand_2)

    # 点击确定
    def click_brand_btn(self):
        self.click(self.brand_btn)
