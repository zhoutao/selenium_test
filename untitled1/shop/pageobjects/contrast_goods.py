# coding=utf-8
from shop.framework.base_page import BasePage


class Contrast_goods(BasePage):

    """
    商品对比：
    点击对比：底部显示 该商品已添加到对比列表中
    添加多个商品，点击对比：跳转到商品对比页面，页面显示该商品的各个信息
    点击删除
    点击关注
    点击加入购物车
    点击立即查看
    """

    # 点击第一件商品下面的对比按钮
    goods_el1 = "x,//span[3]/span"

    # 点击第二件商品下面的对比按钮
    goods_el2 = "x,//li[5]/span/div/span[3]"

    # 两件商品进行对比
    contrast_btn = "s,.el-button--success"

    # 关注
    goods_gz = "i,collectGood_21549"

    # 删除
    goods_del = "x,//td[2]/div/div[4]/a[2]"

    # 加入购物车
    goods_add = "s,.gid_24523"

    # 立即查看按钮
    goods_text = "x,//div[4]/a"

    # 商品名称
    goods_name = "s,.goods__desc__name"

    # 点击第一件商品下面的对比按钮
    def click_goods_el1(self):

        self.switch_window()

        self.sleep(2)

        self.click(self.goods_el1)

        self.sleep(2)

    def click_goods_el2(self):

        self.click(self.goods_el2)

        self.sleep(2)

    # 点击对比按钮
    def click_contrast_btn(self):
        self.click(self.contrast_btn)

    # 点击关注
    def click_goods_gz(self):

        self.under()

        self.click(self.goods_gz)

    # 判断关注按钮
    def get_text_goods_gz(self):
        return self.get_text(self.goods_gz)

    # 点击加入购物车
    def click_goods_add(self):
        self.click(self.goods_add)

        self.sleep(3)

    # 点击删除
    def click_goods_del(self):
        self.top()

        self.click(self.goods_del)

        self.sleep(3)

    # 点击立即查看
    def click_goods_text(self):

        self.click(self.goods_text)

        self.sleep(3)

        self.switch_window()

    # 商品名称判断
    def get_text_goods_name(self):
        return self.get_text(self.goods_name)




