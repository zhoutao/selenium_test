# coding=utf-8
from shop.framework.base_page import BasePage


class Filter_goods(BasePage):
    """

    筛选商品步骤：
    点击价格
    输入价格区间1-2，点击确定
    点击列表
    点击大图
    点击关注
    点击加入购物车

    """

    # 定位器
    # 价格
    price = "s,.price-price > span"

    # 价格区间
    price_large = "s,.input:nth-child(1) > input"

    price_little = "s,.input:nth-child(3) > input"

    # 确定按钮
    price_btn = "s,.price-btn"

    # 列表
    list_1 = "s,.icon-liebiao1"

    # 大图
    big_page = "s,.icon-liebiao"

    # 第一件商品
    goods_one = "i,price-518460"

    # 第二件商品
    goods_two = "i,price-523487"

    # 点击价格
    def click_price(self):
        self.click(self.price)

    # 输入价格区间1-2，
    def type_price_large(self, num):
        self.click(self.price_large)

        self.type(self.price_large, num)

    def type_price_little(self, num):
        self.click(self.price_large)

        self.type(self.price_little, num)

    # 点击确定
    def click_price_btn(self):
        self.click(self.price_large)

        self.click(self.price_btn)

    # 点击列表
    def click_list_1(self):
        self.click(self.list_1)

    # 点击大图
    def click_big_page(self):
        self.click(self.big_page)

    # 返回第一件和第二件商品的价格
    def get_text_goods_one(self):
        return self.get_text(self.goods_one)

    def get_text_goods_two(self):
        return self.get_text(self.goods_two)
