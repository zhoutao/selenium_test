# coding=utf-8
from shop.framework.base_page import BasePage


class Spot_goods_Logistics(BasePage):
    """
    现货商品下单流程：
    点击个人防护，
    点击指定商品图片，跳转到商品详情页面
    点击立即购买
    选择配送方式为物流配送
    选择物流配送地址和具体的物流，点击确定
    代发货选择是
    点击选择代发货信息
    点击提交订单
    """

    # 定位器
    logistics = "s,.dispatching-l:nth-child(2)"

    logistics_address = "s,.el-form-item:nth-child(2) .el-input__inner"

    logistics_address1 = "x,//div[10]/div/div/ul/li"

    city_btn = "s,.el-cascader .el-input__inner"

    province = "x,//div[9]/div/div/div/ul/li/span"

    city = "x,//div[2]/div/ul/li/span"

    area = "x,//div[3]/div/ul/li/span"

    logistics_address_btn = "s,.logistic-button"

    forwarding = "s,.replace > li:nth-child(1)"

    forwarding_user = "s,.el-table__row:nth-child(1) a:nth-child(1)"

    goods_btn2 = "s,.btn-sub"

    add_true2 = "s,p:nth-child(1) > span"

    # 选择配送方式为物流配送
    def click_logistics(self):
        self.click(self.logistics)

        self.wait(2)

    # 点击选择省市区
    def click_city_btn(self):
        self.click(self.city_btn)

        self.sleep(2)

    # 点击省
    def click_city_province(self):
        self.click(self.province)

    # 点击市
    def click_city(self):
        self.click(self.city)

    # 点击区
    def click_area(self):
        self.click(self.area)

    # 选择物流配送地址
    def click_logistics_address(self):
        self.click(self.logistics_address)

        self.sleep(1)

    def click_logistics_address1(self):
        self.click(self.logistics_address1)

        self.sleep(2)

    # 关闭选择物流配送弹窗
    def click_logistics_address_btn(self):
        self.click(self.logistics_address_btn)

        self.sleep(2)

    # 选择代发货
    def click_forwarding(self):
        self.click(self.forwarding)

    # 选择代发货用户
    def click_forwarding_user(self):
        self.click(self.forwarding_user)

        self.sleep(2)

    # 提交订单
    def click_goods_btn2(self):
        self.click(self.goods_btn2)

        self.sleep(5)

    # 检测是否提交订单成功
    def get_text_add_true2(self):
        return self.get_text(self.add_true2)







