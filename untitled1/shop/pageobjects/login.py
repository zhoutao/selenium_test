# coding=utf-8
from shop.framework.base_page import BasePage


class Login(BasePage):

    """
    登录流程：
    点击登录，输入手机号，密码，点击登录按钮
    """

    # 登录
    login_t = "s,.login:nth-child(2)"

    # 手机号
    phone = "i,user_name"

    # 密码
    pwd = "i,password"

    # 点击登录按钮
    login_btn = "s,.login-button"

    # 判断是否登录成功
    phone_test = "s,.header_left > a:nth-child(2)"

    # 点击登录
    def click_login_t(self):

        self.sleep(3)
        self.click(self.login_t)

    # 输入手机号
    def type_phone(self, text):
        self.type(self.phone, text)

    # 输入密码
    def type_pwd(self, text):
        self.type(self.pwd, text)

    # 点击提交按钮
    def click_login_btn(self):
        self.click(self.login_btn)

    # 判断是否登录成功
    def get_text_phone(self):
        return self.get_text(self.phone_test)



        

