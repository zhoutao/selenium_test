# coding=utf-8
from shop.framework.base_page import BasePage


class Goods(BasePage):
    """
    商品详情页面：
    点击个人防护，
    点击指定商品图片，跳转到商品详情页面
    点击关注
    点击加入购物车
    输入数量
    点击立即购买，跳转到提交订单页面
    选择 选择发货方式
    点击提交订单

    """
    # 定位器
    # 切换工艺品
    crafts = "s,.banner_list > .banner_title"

    # 清洁
    my_brand = "s,.a-center:nth-child(2) > a:nth-child(2)"

    # 指定商品图片
    good_pict = "x,//div[4]/p[2]"

    # 关注
    good_collect = "i,collect"

    # 加入购物车
    good_add_cart = "s,.cart"

    # 数量
    good_num = "s,.el-input--mini > .el-input__inner"

    # 立即购买
    good_buy = "s,.el-button--danger"

    # 发货方式
    ship_free = "s,.dispatching-main:nth-child(1) li:nth-child(1)"

    # 提交订单
    add_order = "s,.btn-sub"

    # 检测按钮是否为已关注
    collect_true = "s,#collect > span"

    # 检测是否提交订单成功
    add_true = "s,p:nth-child(1) > span"

    # 切换工艺品
    def switch_crafts(self):

        self.click(self.crafts)

        self.switch_window()

    # 点击清洁
    def click_my_brand(self):

        self.click(self.my_brand)

    # 点击指定商品图片
    def click_good_pict(self):

        self.switch_window()

        self.sleep(2)

        self.click(self.good_pict)

    # 点击关注
    def click_good_collect(self):

        self.sleep(2)

        self.click(self.good_collect)

        self.sleep(4)

    # 点击加入购物车
    def click_good_add_cart(self):
        self.click(self.good_add_cart)

    # 输入数量
    def type_good_num(self, num):
        self.type(self.good_num, num)

    # 点击立即购买
    def click_good_buy(self):
        self.click(self.good_buy)

    # 选择发货方式
    def click_ship_free(self):

        self.sleep(5)

        self.click(self.ship_free)

        self.sleep(1)

    # 点击提交订单
    def click_add_order(self):
        self.click(self.add_order)

        self.sleep(5)

    # 检测按钮是否为已关注
    def get_text_collect_true(self):
        return self.get_text(self.collect_true)

    # 检测是否提交订单成功
    def get_text_add_true(self):

        return self.get_text(self.add_true)
