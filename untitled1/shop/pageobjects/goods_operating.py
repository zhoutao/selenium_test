# coding=utf-8
from shop.framework.base_page import BasePage


class Goods_operaing(BasePage):

    """
    商品列表页面：
    点击关注
    点击加入购物车
    点击立即查看
    """
    # 定位器

    # 关注
    goods_gz2 = "s,.goods-collect-21549"

    # 选择多件商品加入购物车
    # goods1_join_cart = "x,//li[3]/span/div[5]/span/span"
    goods1_join_cart = "s,.list-goods-body:nth-child(5) .setting:nth-child(1) > span"

    goods2_join_cart = "s,.list-goods-body:nth-child(8) .setting:nth-child(1) > span"

    goods3_join_cart = "s,.list-goods-body:nth-child(10) .setting:nth-child(1) > span"

    # 关闭加入购物车成功的弹窗
    # join_cart_close = "s,.el-dialog__close"
    join_cart_close = "x,//button/i"

    # 查看详情
    goods_details = "x,//li[3]/span/div/span/span"

    # 判断跳转成功
    goods_name = "s,.goods__desc__name"

    # 点击关注
    def click_goods_gz2(self):

        self.switch_window()

        self.click(self.goods_gz2)

    # 选择多件商品加入购物车
    def click_goods1_join_cart(self):
        # self.click(self.goods2_join_cart)
        self.click(self.goods1_join_cart)

        self.sleep(2)

    # 关闭加入购物车成功的弹窗
    def click_join_cart_close(self):

        self.click(self.join_cart_close)

        self.sleep(5)

    def click_goods2_join_cart(self):

        self.sliding()

        self.click(self.goods2_join_cart)

        self.sleep(2)

    def click_join_cart_close1(self):

        self.click(self.join_cart_close)

        self.sleep(2)

    def click_goods3_join_cart(self):

        self.sleep(2)

        self.click(self.goods3_join_cart)

        self.sleep(2)

    def click_join_cart_close2(self):

        self.click(self.join_cart_close)

        self.sleep(2)

    def click_goods_details(self):

        self.click(self.goods_details)

        self.sleep(2)

    def get_goods_name(self):
        return self.get_text(self.goods_name)

