# coding=utf-8
from shop.framework.base_page import BasePage


class Spot_goods(BasePage):
    """
    现货商品下单流程：
    点击个人防护，
    点击指定商品图片，跳转到商品详情页面
    选择配送方式为自提门店
    订单留言
    点击提交订单

    """
    # 定位器

    # 切换工艺品
    crafts = "s,.banner_list > .banner_title"

    my_brand1 = "s,.a-center:nth-child(2) > a:nth-child(2)"

    good_pict1 = "x,//li[5]/span/a/div/img"

    good_buy1 = "s,.buy"

    store = "s,.dispatching-l:nth-child(1)"

    store1 = "s,.dispatching-l:nth-child(1) li:nth-child(1)"

    leave = "s,.el-textarea__inner"

    goods_btn1 = "s,.btn-sub"

    add_true1 = "s,p:nth-child(1) > span"

    # 点击清洁

    def switch_crafts(self):

        self.click(self.crafts)

        self.switch_window()

    def click_my_brand1(self):
        self.click(self.my_brand1)

        self.switch_window()

        self.sleep(2)

    # 点击指定商品图片
    def click_good_pict1(self):
        self.click(self.good_pict1)

        self.sleep(2)

    # 点击立即购买
    def click_good_buy1(self):
        self.click(self.good_buy1)

        self.sleep(2)

    # 选择自提门店
    def click_store(self):
        self.click(self.store)

        self.sleep(2)

    def click_store1(self):
        self.click(self.store1)

    # 订单留言
    def type_leave(self, text):
        self.type(self.leave, text)

        self.sleep(1)

    # 提交订单
    def click_goods_btn1(self):
        self.click(self.goods_btn1)

        self.sleep(5)

    # 检测是否提交订单成功
    def get_text_add_true1(self):
        return self.get_text(self.add_true1)


